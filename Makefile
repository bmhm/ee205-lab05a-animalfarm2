###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Brooke Maeda <bmhm@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   11 February 2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	g++ -o main main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	
clean:
	rm -f *.o main


